# IPv6 PTR Generator

In order to provide hostnames for IPv4 addresses in a given network, one used to pre-generate 
all possible records in advance with BIND's `$GENERATE` directives for both reverse (`IN PTR`) and
forward (`IN A`) records.

This approach is impractical in IPv6 world. Even the smallest /64 network contains 2^64 addresses,
and twice as many records, which will require more RAM that anyone would be willing to spend on it.

Furthermore, IPv6 address utilization is very sparse (by design), so most of those records will
never be used.

One of the approaches suggested in [RFC 8501](https://www.rfc-editor.org/rfc/rfc8501.html) is
generating dynamic records on demand, the goal of this project is to implement just that.


## TODO
- start with tests
- for each zone, define a pair of prefix and domain
- each address should correspond to exact one hostname, and vice versa
- mapping options:
    - literal (i.e. `bde7:f427:adcb:a72d` becomes `bde7f427adcba72d`)
    - [harsh](https://github.com/archer884/harsh/)
- automated deployment (options?)
- usage instructions (in this readme)


## Example
Prefix: 2001:db8:77a2:ad0c::/64  
Domain: ad0c-77a2.example.org  

Request: `d.2.7.a.b.c.d.a.7.2.4.f.7.e.d.b.c.0.d.a.2.a.7.7.8.b.d.0.1.0.0.2.ip6.arpa. IN PTR`  
Response: `... IN PTR bde7f427adcba72d.ad0c-77a2.example.org.`  

Request: `bde7f427adcba72d.ad0c-77a2.example.org. IN AAAA`  
Response: `... IN AAAA 2001:db8:77a2:ad0c:bde7:f427:adcb:a72d`
